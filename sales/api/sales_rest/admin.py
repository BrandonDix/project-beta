from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import SalesPerson, PotentialCustomer, SalesRecord, AutomobileVO

@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    pass

@admin.register(PotentialCustomer)
class PotentialCustomer(admin.ModelAdmin):
    pass

@admin.register(SalesRecord)
class SalesRecord(admin.ModelAdmin):
    pass

@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass