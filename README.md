# CarCar

Team:

Brandon - Service/Inventory
David - Sales/Inventory

## Design

## Service microservice
AutomobileVo Model:
 - color
 - year
 - vin

Appointment Model:
 - Automobile (foreign key to Automobile Model)
 - Owner
 - Date and Time of appointment
 - Tech object
 - Reason for service appointment
 - Automobile object
 - finished
 - canceled
 - vip

Tech Model:
 - name
 - id

Bounded Context: (Service) / (Sales) / (Inventory)

Looking forward the services will have more to implement within that context that will add more reason for it to be separated and same with sales, I imagine sales will have to handle payment and those sorts of things in the future.

Aggregate root: Dealership - { services, sales, inventory }

Aggregates: 

Service:
 - Entities:
    - Appointments 
    - Technicians
 - Value Objects:
    - AutomobileVO is a value object 

Inventory: 
 - Entities:
    - Automobile 
 - Value Objects:
    - Manufacturer
    - Vehicle Models

Sales:
 - Entities:
    - Sales Person
    - Sales Record
    - Customer
 - Value Objects:
    - AutomobileVO


Aggregate Root Service:

This service will be polling from the inventory microservice to obtain the Automobile model data. We will use that objects VIN property to uniquely identify the vehicle being serviced within the service appointment system. We are creating two separate models because we want data tables for the technician and the appointment to be separate. This microservice is designed to control the data flow/display of appointments made by a service employee. It takes data requests via form to schedule an appointment within our server. We assign a tech at this time, then we display the appointments in a list so our employees can have access to all the necessary properties of the appointment instance. We can also filter out that data based on the VIN of the vehicle being serviced and display the history of the vehicles services. When an employee is looking at a service appointment they can interact with the data by submitting a "finished" or "canceled" "put" request back to our database to change that boolean field to either True for "finished" or "canceled". This will be something our dev team can see but the requirements of this project in my opinion, should have had this displayed in the service history list. *This will be a change I implement going forward. The technician data flow is simply to create a technician with their ID in our system and to assign then to appointments.

Appointment is an Entity because it can be continuously identified and it also an object that goes through a life cycle decided either by "finished" status, "canceled" status or removal from the database

Technician is an Entity because it can be continuously identified and it also an object that goes through a life cycle decided either by removal from the database, (i.e. fired/quit)

Automobile is a Value Object as described by its name. It is a value object created in our respective micro services to pull the necessary data from the Automobile Object with in the inventory bounded context.

Takeaway:

 - Planning is important. Really read through the requirements and make a plan before you are coding. 
 - It is easy to crete your own functions that can help towards a desired goal.

Link to Excalidraw Model []:
https://app.excalidraw.com/l/AKyBOkrVaFQ/Aj6pLqK6aVO


## Sales microservice

For the sales microservice, it required me to be able to add a sales person, add a customer, record a sale, list all sales, and get a specific sales person's history. Knowing these requirements, I knew that in order to do anything in sales would require automobile information from the Inventory API. Having automobile information allows me to create a sales record, list all sales, and get specific sales information for a sales person. By creating an Automobile value object model in the Sales application, I could then poll the Inventory API for all of its automobile data and use it in the Sales microservice. 

By identifying Inventory, Sales, and Services as the aggregate root it provided a better understanding of how to proceed. For the Sales microservice, the AutomobileVO, SalesPerson, PotentialCustomer, and SalesRecord models were going to be the aggregates, because all of these in different combinations allowed me to use methods on the Sales aggregate root, such as creating a sale, listing a sale, etc. I also identified SalesPerson and PotentialCustomer as entities, because while they have similar properties (i.e: name), they are not the same thing. They have continuous identity because even though the salesperson and customer could change their name, they are still the same salesperson and the same customer. SalesRecord would be identified as a value object, because there is no life cycle on it. It simply exists with the recorded information.

A few decisions I was faced with when building this program, was the multitude of links that we had going on required us to research an effective navigation bar. Putting the links with their respective dropdown category helped keep things clear and concise from both a user and developer perspective. While most of our "list" pages use cards to display the material, I decided that putting sales person data into a traditional table would be a more effective route for displaying the information, since there was quite a bit of info to display. A big challenge was getting the "Record a new sale" page to have the three dropdown menus and putting all of that code into the componentDidMount and formatting it correctly. Overall, I have learned a lot more about the way React works with the different components and have become more familiar with the specific quirks and errors that occur. 











