import React from 'react';

class TechForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            id: ''
        }
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleIdChange = this.handleIdChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleNameChange(event){
        const value = event.target.value
        this.setState({name: value})
    }
    handleIdChange(event){
        const value = event.target.value
        this.setState({id: value})
    }
    async handleSubmit(event){
        event.preventDefault()
        const data = {...this.state}
        const TechUrl = 'http://localhost:8080/api/tech/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(TechUrl, fetchConfig)
        if (response.ok){
            const newTech = await response.json()
           
            const cleared = {
                name: '',
                id: ''
            }
            this.setState(cleared)
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Employee Enrollment</h1>
                        <form onSubmit={this.handleSubmit} id="create-tech-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="fabric">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleIdChange} value={this.state.id} placeholder="id" required type="text" name="id" id="id" className="form-control" />
                                <label htmlFor="style_name">Employee ID</label>
                            </div>
                           
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default TechForm;