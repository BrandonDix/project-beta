import React from 'react';


function AppointmentColumn(props) {
    return (
        <div className="col">
            {props.list.map(appoint => {
                return (
                    <div key={appoint.href} className="card mb-3 shadow">
                        <div className="card-body">
                            <ul className="list-unstyled">
                                <h2 className="card-title">VIN # {appoint.automobile.vin}</h2>
                                <ul>
                                    <li><strong>Owners Name:</strong><p>{appoint.owner}</p></li>
                                    <li><strong>Reason for Service:</strong><p>{appoint.reason}</p></li>
                                    <li><strong>Technician:</strong><p>{appoint.tech.name}</p></li>
                                    <li><strong>Date:</strong><p>{appoint.date}</p></li>
                                    <li><strong>Time:</strong><p>{appoint.time}</p></li>
                                    {vip(appoint)}
                                </ul>
                            </ul>
                            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                                <button onClick={() => finished(appoint)} type="button" className="btn btn-outline-success">Finished</button>
                                <button onClick={() => canceled(appoint)} type="button" className="btn btn-outline-danger">Cancel</button>
                            </div>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}
{/* <img src={appoint.picture} className="card-img-top" /> */ }

function vip(appoint){
    if (appoint.vip === true){
        return (
            <li><strong>VIP!</strong><p>Owner has purchased vehicle with this dealership</p></li>
        )
    }
}

async function finished(appoint) {
    const data = { finished: "True" }
    const deleteURL = `http://localhost:8080/api/appointment/${appoint.id}/`
    const fetchConfig = {
        method: "put",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    
    const response = await fetch(deleteURL, fetchConfig)
    if (response.ok) {
        console.log("appointment finished", response)
    }
}

async function canceled(appoint) {
    const data = { canceled: "True" }
    const deleteURL = `http://localhost:8080/api/appointment/${appoint.id}/`
    const fetchConfig = {
        method: "put",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    
    const response = await fetch(deleteURL, fetchConfig)
    if (response.ok) {
        console.log("appointment cancelled", response)
    }
}


class AppointmentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointCollumn: [[], []],
        };
    }


    async componentDidMount() {
        const url = 'http://localhost:8080/api/appointment/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                
                const data = await response.json();
                
                const requests = [];
                for (let appoint of data.appointment) {
                    if (appoint.finished === false) {
                        if (appoint.canceled !== true) {
                            const detailUrl = `http://localhost:8080/api/appointment/${appoint.id}/`;
                            console.log(detailUrl)
                            requests.push(fetch(detailUrl));
                        }

                    }

                }
                const responses = await Promise.all(requests);
                const appointCollumn = [[], []];
                let i = 0;
                for (const appointResponse of responses) {
                    if (appointResponse.ok) {
                        const details = await appointResponse.json();
                        appointCollumn[i].push(details);
                        i = i + 1;
                        if (i > 1) {
                            i = 0;
                        }
                    } else {
                        console.error(appointResponse);
                    }
                }

                this.setState({ appointCollumn: appointCollumn });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <>
                <div className="px-4 py-5 my-5 mt-0 text-center bg-light">
                    <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                    <h1 className="display-5 fw-bold">Appointments</h1>
                    <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4">
                            Take a look at our Appointments.
                        </p>
                    </div>
                </div>
                <div className="container">
                    <h2>Upcoming:</h2>
                    <div className="row">
                        {this.state.appointCollumn.map((appointList, index) => {
                            return (
                                <AppointmentColumn key={index} list={appointList} />
                            );
                        })}
                    </div>
                </div>
            </>
        );
    }
}

export default AppointmentList;